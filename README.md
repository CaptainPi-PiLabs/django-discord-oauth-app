# Django-Discord OAuth app #

This app will override the user model and allow login though the discord OAuth api

## Exteneral libaries ##
* requests_oauthlib - https://requests-oauthlib.readthedocs.io/

## Changes to project files ##
Firstly you must create a discord app and put your callback path in the allowed redirects in the OAuth2 page.

E.g. `http://127.0.0.1:8000/profile/oauth`

Link to discord developer page. https://discordapp.com/developers/applications/

You will need to add these modifications to these files in the main project directory.
### settings.py ###
```python
INSTALLED_APPS = [
	...
	'discordAuth',
	...
]

AUTH_USER_MODEL = 'discordAuth.CustomUser'

AUTHENTICATION_BACKENDS = [
	'discordAuth.backends.DiscordAuthBackend'
]

DISCORD_CLIENT_SECRET = 'discord app secret here'
DISCORD_CLIENT_ID = 'discord app client id'
DISCORD_REDIRECT_URI = 'uri of callback address. E.g. http://127.0.0.1:8000/profile/oauth'
```
### urls.py ###
```python
urlpatterns = [
	...
	path('profile/', include('discordAuth.urls')),
	...
]
```

### discordAuth/views.py ###
By default the connection is not under SSL. To change this remove this line.
```python
os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'
```

## Special thanks ##
Some credit should go to NanoDano whos code I based some of this off. https://www.devdungeon.com/

## Extras ##
If you have any suggestions you can either add to the issue tracker here or find me with CaptainPi#7358 on discord.

* This app is entierly backend and contains no html.
* Currently checks for the state change aren't done in views.py which leaves connections open to attack.

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from discordAuth.models import CustomUser


class CustomUserAdmin(UserAdmin):
    model = CustomUser

    def username(self, CustomUser):
        return CustomUser.username

    list_display = ('discord_id', 'username', 'discord_discriminator', 'email', 'is_active', 'is_staff', 'is_superuser')
    list_filter = ('username', 'email')

    fieldsets = (
        (None, {'fields': ('discord_id', 'email')}),
        ('Personal info', {'fields': ('username', 'discord_discriminator')}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser')})
    )
    add_fieldsets = (
        (None, {'classes': ('wide'),
                'fields': ('discord_id', 'username', 'discord_discriminator',
                           'email', 'is_active', 'is_staff', 'is_superuser')})
    )
    search_fields = ('email', 'username', 'discord_id')
    ordering = ('username', 'discord_id')
    filter_horizontal = ()


admin.site.register(CustomUser, CustomUserAdmin)

from django.apps import AppConfig


class DiscordauthConfig(AppConfig):
    name = 'discordAuth'

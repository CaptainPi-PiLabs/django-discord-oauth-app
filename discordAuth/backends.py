from django.contrib.auth.backends import BaseBackend

from discordAuth.models import CustomUser


class DiscordAuthBackend(BaseBackend):
    def authenticate(self, request, discord_id=None):
        try:
            user = CustomUser.objects.get(discord_id=discord_id)
            return user
        except CustomUser.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return CustomUser.objects.get(pk=user_id)
        except CustomUser.DoesNotExist:
            return None

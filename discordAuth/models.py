# discordAuth/models.py
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import Group, Permission, PermissionsMixin
from django.db import models


class CustomUserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, id, username, discriminator, email, **extra_fields):
        user = self.model(email=self.normalize_email(email), **extra_fields)
        user.discord_id = id
        user.username = username
        user.discord_discriminator = discriminator
        print("====================")
        print("Creating new user. ID: %s" % id)
        print("====================")
        user.save(using=self._db)

    def create_user(self, id, user, discriminator, email, **extra_fields):
        return self._create_user(id, user, discriminator, email, **extra_fields)

    def create_superuser(self, id, user, discriminator, email, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        return self._create_user(id, user, discriminator, email, **extra_fields)


class CustomUser(AbstractBaseUser, PermissionsMixin):
    discord_id = models.IntegerField(unique=True, primary_key=True)
    username = models.CharField(max_length=32)
    discord_discriminator = models.IntegerField()
    email = models.EmailField(('Email address'), unique=True)
    last_login = models.DateTimeField(auto_now=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    objects = CustomUserManager()

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'discord_id'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = ('user')
        verbose_name_plural = ('users')

    def get_id(self):
        return self.discord_id

    def get_username(self):
        return self.username

    def get_user(self):
        return self.discord_id + self.discord_discriminator

    def get_email(self):
        return self.email

    @property
    def active(self):
        return self.is_active

    @property
    def staff(self):
        return self.is_staff

    @property
    def superuser(self):
        return self.is_superuser

    def pm_user(self):
        print("Sending discord message to user")

    def email_user(self):
        print("Sending email message to user")

    def __str__(self):
        return self.get_id()

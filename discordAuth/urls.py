from django.urls import path, include

from .views import login_discord, login_callback, logout_discord

urlpatterns = [
    path('login/', login_discord, name='Discord login'),
    path('logout/', logout_discord, name='Logout'),
    path('oauth/', login_callback, name='Discord OAuth server')
]

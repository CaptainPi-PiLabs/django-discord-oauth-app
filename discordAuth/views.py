# discordAuth/views
from django.contrib.auth import login, logout
from django.http import HttpResponseRedirect, HttpResponse
from requests_oauthlib import OAuth2Session
import os

from ProjectName import settings
from .backends import DiscordAuthBackend
from .models import CustomUserManager, CustomUser

os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'

base_discord_api_url = 'https://discordapp.com/api'
scope = ['identify', 'email']
token_url = 'https://discordapp.com/api/oauth2/token'
authorize_url = 'https://discordapp.com/api/oauth2/authorize'


def login_discord(request):
    oauth = OAuth2Session(settings.DISCORD_CLIENT_ID, redirect_uri=settings.DISCORD_REDIRECT_URI, scope=scope)
    login_url, state = oauth.authorization_url(authorize_url)
    request.session['state'] = state
    return HttpResponseRedirect(login_url)


def login_callback(request):
    discord = OAuth2Session(client_id, redirect_uri=settings.DISCORD_REDIRECT_URI, state=request.session['state'], scope=scope)
    token = discord.fetch_token(
        token_url,
        client_secret=settings.DISCORD_CLIENT_SECRET,
        authorization_response=request.get_full_path()
    )
    request.session['discord_token'] = token
    response = discord.get(base_discord_api_url + '/users/@me')
    user = DiscordAuthBackend().authenticate(request, discord_id=response.json()['id'])

    if user is not None:
        print("User found")
        login(request, user)
    else:
        print("User not found")
        discord_id = response.json()['id']
        user = response.json()['username']
        disc = response.json()['discriminator']
        email = response.json()['email']
        CustomUser.objects.create_user(discord_id, user, disc, email)
        newuser = DiscordAuthBackend.authenticate(request, discord_id=discord_id)
        login(request, newuser)

    return HttpResponse('You have logged in. Profile: %s' % response.json()['id'])


def logout_discord(request):
    logout(request)
    return HttpResponse('You are now logged out.')
